import XMonad
import qualified XMonad.StackSet as W
import XMonad.Actions.CycleWS  
import XMonad.Actions.SpawnOn  
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName  
import XMonad.Layout.ToggleLayouts
import XMonad.Util.EZConfig (additionalKeys,additionalMouseBindings)  
import XMonad.Hooks.DynamicLog
import XMonad.Layout.IndependentScreens (countScreens)
import XMonad.Util.Run
import System.IO (Handle, hPutStrLn) 

--https://thinkingeek.com/2011/11/21/simple-guide-configure-xmonad-dzen2-conky/
--https://www.reddit.com/r/xmonad/comments/rqqg8d/several_monitors_xmobar_issue/  
--TODO
--Add 0 workspace

myKeys =
  -- Programs
  [ ((mod4Mask, xK_f), spawn "firefox")
  , ((mod4Mask .|. shiftMask, xK_Return),
     spawnAndDo (doF (W.focusDown . W.swapMaster . W.focusDown)) ("konsole"))
  , ((mod4Mask, xK_e), spawn "emacs")
  , ((mod4Mask, xK_m), spawn "thunderbird")
  , ((mod4Mask, xK_l), spawn "libreoffice")
  , ((mod4Mask, xK_s), spawn "systemsettings5")
  , ((mod4Mask, xK_o), spawn "octave --gui")
  , ((mod4Mask, xK_t), spawn "~/.Telegram/Telegram")
  , ((mod4Mask, xK_k), spawn "keepassxc")
  , ((mod4Mask, xK_g), spawn "gnucash")
  -- Configs
  , ((mod4Mask .|. shiftMask, xK_r), spawn "xmonad --restart")
  , ((mod4Mask .|. shiftMask, xK_c), spawn "emacs ~/Git/AlecisLaptop")
  , ((mod4Mask, xK_c), spawn "emacs ~/Git/AlecisLaptop/Xmonad/xmonad.hs")
  -- Pause session
  , ((mod4Mask .|. shiftMask, xK_Escape), spawn "echo disk > /sys/power/state")
  , ((mod4Mask .|. shiftMask, xK_b),
     spawn "i3lock -i ~/Pictures/Wallpapers/CamaleonteI3.png && echo mem > /sys/power/state")
  -- Screenshots
  , ((0, xK_Print), spawn "gnome-screenshot")
  , ((mod4Mask .|. shiftMask, xK_F7), spawn "gnome-screenshot -w")
  , ((mod4Mask, xK_F7), spawn "gnome-screenshot -a")
  -- Brightness control
  , ((mod4Mask, xK_F5), spawn "brightnessctl set 10%-")
  , ((mod4Mask, xK_F6), spawn "brightnessctl set +10%")
  -- Dolphin
  , ((mod4Mask .|. shiftMask, xK_h), spawn "dolphin ~/.")
  , ((mod4Mask .|. shiftMask, xK_g), spawn "dolphin ~/Git")
  , ((mod4Mask .|. shiftMask, xK_p), spawn "dolphin ~/Aleci/Università/PoliTO")
  , ((mod4Mask .|. shiftMask, xK_d), spawn "dolphin ~/Downloads")
  -- Windows, workspaces and Layouts
  , ((mod4Mask, xK_w), kill)
  , ((mod4Mask, xK_space), sendMessage (Toggle "Full") )
  , ((mod4Mask .|. shiftMask, xK_space), sendMessage NextLayout)
  , ((mod4Mask , xK_Return), windows W.swapMaster)
  , ((mod4Mask .|. shiftMask, xK_Tab), windows W.focusDown)
  , ((mod4Mask, xK_Tab), toggleWS)
  -- Trackpad disable
  , ((mod4Mask .|. shiftMask, xK_F11), spawn "xinput --disable 11")
  , ((mod4Mask, xK_F11), spawn "xinput --enable 11")
  ]

myMouse =
  -- Mod + right click = drag window
  [ ((mod4Mask,button2), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))
  -- Mod + left click = resize window
  , ((mod4Mask,button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
  ]

myStartupHook = do
  spawn "xsetroot -cursor_name left_ptr"
  --Enable Matlab Gui on Xmonad
  setWMName "LG3D"

myWorkspaces = ["1.Firefox", "2.Dolphin", "3.Emacs", "4.PDF", "5.Mail", "6.Shell", "7.Matlab", "8.Simulink", "9.Shell"]

-- To configure the autostarted applets go in the X11 folder
myLogHook xmobarPipes = do
  mapM_ myLogHookAux xmobarPipes
    where myLogHookAux xmobarPipe = do dynamicLogWithPP $ def { ppOutput = hPutStrLn xmobarPipe }

myConfig xmobarPipes = def
  { modMask     = mod4Mask -- set 'Mod' to windows key
  , manageHook  = manageSpawn <+> manageDocks
  , startupHook = myStartupHook -- Can't work on hybrid systems without setting the guix env vars
  , workspaces = myWorkspaces
  , handleEventHook = docksEventHook
  , logHook = myLogHook xmobarPipes
  , layoutHook  =  avoidStruts $ (toggleLayouts Full
                               (Tall 1 (3/100) (3/5) ||| Mirror (Tall 1 (3/100) (3/5))))
  } `additionalKeys` myKeys `additionalMouseBindings` myMouse

main = do
  n <- countScreens
  xmobarPipes <- mapM (\i -> spawnPipe ("xmobar -x " ++ (show i))) [0..n-1]
  xmonad $ myConfig xmobarPipes

