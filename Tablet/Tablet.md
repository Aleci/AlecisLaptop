# Wacom X200's tablet configurations
My preinstalled Trisquel had an out of the box working tablet, so I had to configure only the screen rotation.   
Using the command `xrandr` you can turn the screen and with the command `xsetwacom` you can turn the tablet.   
You need to rotate both if you want to avoid problems using the stylus.  
The command `xsetwacom` require the corret name of the Wacom tablet, you can find it using the command `xinput`.  
I like to rotate the screen to take notes with Xournal++ so I need only two orientations: the normal and the left one.  
I wrote a little script to toggle the orientations (Toggle90.sh) and i bound it to a button on the monitor.  
If you need some more reference you can start from here:  
https://www.suse.com/documentation/opensuse121/book_opensuse_reference/data/sec_tablet_trouble.html   
