(specifications->manifest
 ;; ControlSystem package not installable at least on hybrid systems
 ;; For now go in the official website, untar the archive in /opt and link bin/julia in /usr/bin
  '("julia" "suitesparse"))
