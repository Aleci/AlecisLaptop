#!/bin/bash

# Use the profile filename without extension as unique argument of the script
# Example: . installProfile.sh Emacs

GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
mkdir -p "$GUIX_EXTRA_PROFILES"/$1
guix package --manifest=$1.scm --profile="$GUIX_EXTRA_PROFILES"/$1/$1
