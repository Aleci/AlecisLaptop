(specifications->manifest
 '("acpi"  ; required in the modbar
   "grip"  ; required by grip-mode (Markdown)
   "libtool" ; required by julia-snail
   "python-pygments" ; required by org export latex code block syntax highlight
   ;clang ; required by lsp-mode (install clang and clangd from apt)
   ;julia ; required by julia-snail (install julia from main website)
   "emacs"))
