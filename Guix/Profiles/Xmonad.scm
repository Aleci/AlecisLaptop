(specifications->manifest
 '("cabal-install" ; "ghc" "xmonad" "ghc-xmonad-contrib"   ; Base Xmonad package
   "xmobar" "stalonetray" "dmenu"        ; Almost mandatory add-ons
   ; Configuration specific packages
   ;"network-manager-applet" "redshift" "redshift:gtk" ; Not working packages, using fallback ones
   "volumeicon" "cbatticon" "xinput" "feh" "brightnessctl"))
;kdeconnect

; i3lock requires the setuid-program configuration in the GuixSD system declaration to work
; ghc create linking problems so is better to use the fallback debian package
