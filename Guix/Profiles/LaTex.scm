
; Use fallback apt packages instead:
; texlive-base texlive-bibtex-extra texlive-fonts-recommended
; texlive-latex-base texlive-latex-extra

(specifications->manifest
 '("texlive-base" ; pdflatex
   "texlive-listings" ; listings (seems to be useful...)
   "texlive-hyperref" ; hyperref 
   "texlive-beamer" ; presentations
   "texlive-pgf" "texlive-helvetic")) ; tikz 

; never use texlive, is huge!
