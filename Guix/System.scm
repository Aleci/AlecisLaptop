;; This is an operating system configuration generated
;; by the graphical installer.

(use-modules (gnu))
(use-service-modules desktop networking ssh xorg)

; Default system configs
(operating-system
  (locale "en_US.utf8")
  (timezone "Europe/Rome")
  (keyboard-layout (keyboard-layout "us"))
  (host-name "Istambul")
  (users (cons* (user-account
                  (name "ale")
                  (comment "Aleci")
                  (group "users")
                  (home-directory "/home/ale")
                  (supplementary-groups
                    '("wheel" "netdev" "audio" "video")))
                %base-user-accounts))
  
; Default packages
  (packages
   (append (map specification->package
		(list "xmonad" "ghc" "ghc-xmonad-contrib" "xmobar" "dmenu"
		      "nss-certs" "wget" "vlc" "redshift" "make" "git"
		      "setxkbmap" "xkbcomp"))
	  %base-packages))

; Xorg DE  
  (services
    (append
      (list (service xfce-desktop-service-type)
            (set-xorg-configuration
              (xorg-configuration
                (keyboard-layout keyboard-layout))))
      %desktop-services))

; Partition description  
  (bootloader
    (bootloader-configuration
      (bootloader grub-bootloader)
      (target "/dev/sda")
      (keyboard-layout keyboard-layout)))
  ;
  (mapped-devices
    (list (mapped-device
            (source
              (uuid "acaad91d-a857-4346-88b3-35b09d574d65"))
            (target "cryptroot")
            (type luks-device-mapping))))
  ;
  (file-systems
    (cons* (file-system
             (mount-point "/")
             (device "/dev/mapper/cryptroot")
             (type "ext4")
             (dependencies mapped-devices))
           %base-file-systems)))
