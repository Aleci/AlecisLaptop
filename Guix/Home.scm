; Assumption: The git repo should have this position with respect to the home directory
; $HOME
; └── Aleci
;     └── .home
;         └── Git
;             └── AlecisLaptop

(use-modules (gnu home)
	     (gnu home services)
	     (gnu home services symlink-manager)
	     (gnu home services shells)
             (gnu packages)
             (guix gexp)
	     (gnu services))             

(home-environment
 (services
  (list

   (service home-bash-service-type
            (home-bash-configuration
	     (guix-defaults? #t)
	     (bash-profile
	      (list (local-file "../Bash/Profile.sh" "bash-profile")))
	     (environment-variables
	      (list `("REPO"."$HOME/Aleci/.home/Git/AlecisLaptop")))
             (bashrc
              (list (local-file "../Bash/ImpostazioniBash.sh" "bashrc")))
	     (aliases
	      (list `("reconfigure-home"."cd ; guix home reconfigure $REPO/Guix/Home.scm ; cd -")
		    `("backup". "bash $REPO/Bash/Backup.sh")
		    `("update-sys". "sudo apt update; sudo apt upgrade -y; sudo apt autoremove -y")
		    `("my-ip". "wget myip.datasystems24.de -O /tmp/index.html 2> /dev/null && cat /tmp/index.html && rm /tmp/index.html && echo ''")))))

;;   (simple-service
;;    'x11-config
;;    home-files-service-type
;;    (list `(".xsessionrc", (plain-file "tmpName"
;;				       "file contents"))))

 ;  (simple-service
;    'home-dirs
  (service home-symlink-manager-service-type 
    (list `(symlink "Aleci/.home/Git" "Git")
	  `(symlink "Aleci/.home/Desktop" "Desktop")
	  `(symlink "Aleci/.home/Documents" "Documents")
	  `(symlink "Aleci/.home/Downloads" "Downloads")
	  `(symlink "Aleci/.home/Music" "Music")
	  `(symlink "Aleci/.home/Calibre" "Calibre")
	  `(symlink "Aleci/.home/Pictures" "Pictures")
	  `(symlink "Aleci/.home/Templates" "Templates")
	  `(symlink "Aleci/.home/Videos" "Videos")))

   ;;   (simple-service
   ;;    'user-bin
   ;;    home-symlink-manager-service-type 
   ;;    (list `(symlink "Git/AlecisLaptop/Bash/Backup.sh" "bin/Backup.sh")))

   ;;   (simple-service
   ;;    'user-bin
   ;;   home-files-service-type
   ;;    (list `("bin/Backup.sh", (chmod (local-file "../Bash/Backup.sh" "Backup") "550")
   ;;	    `("bin/Toggle90.sh", (chmod (local-file "../Tablet/Toggle90.sh" "Toggle90") "550")))))
   
   (simple-service
    'x11-config
    home-files-service-type
    (list `(".xsessionrc", (local-file "../X11/xsessionrc.sh" "xsessionrc"))))
   
   ;; (simple-service
   ;;  'emacs-config
   ;;  home-files-service-type
   ;;  (list `(".emacs.d/init.el", (local-file "../Emacs/Init.el" "init.el")))) 

   (simple-service
    'ite-config
    home-files-service-type
    (list `(".xkb/ite", (local-file "../Keyboard/ite/ite.dump" "ite"))))

   (simple-service
    'xmonad-configs
    home-files-service-type
    (list `(".xmonad/xmonad.hs", (local-file "../Xmonad/xmonad.hs" "xmonad.hs"))
	  `(".xmobarrc",(local-file "../Xmonad/xmobarrc" "xmobarrc"))
          `(".stalonetrayrc",(local-file "../Xmonad/stalonetray" "stalonetrayrc")))))))
   
