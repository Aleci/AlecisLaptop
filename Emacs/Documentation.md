# Emacs-Configurations Documentation

## Todo list:

*      redo keychord  
*      throw line keychord
*      swap char keychord
*      uncomment this line (remove //)
*      move current word forward  
*      move current word backward  

## Custom Keychords:  

* jk			jump to headchar  
* jl			jump to line   
* jf			jump to char forward  
* jb			jump to char backward  
* un 			undo

## Custom Keystrokes:

* M-o			open a new line below and place the cursor at the beginning of it (O vim command)  
* C-o			open a new line above and place the cursor at the beginning of it (o vim command)  
* M-RET			newline but the cursor remain at his position (old standard emacs keystroke C-o)  
* M-x save-macro	save a macro in init.el  
* M-x main              add main structure to begin your c code  
* C-c c			compile  
* C-c r			recompile
* C-c t			recompile and go to ansi-term
* M-j 			join next line with the current one
* C-c m m		multicursor: a cursor for every line of region
* C-c m l		multicursor: a cursor on every word like the selected
* C-c m p		multicursor: two cursors, the second on the mark
* C-c m d		multicursor: a cursor for every word like the selected in defun
* C-c m a		multicursor: a cursor for every string of carachter like the selected one
* C-c m b		multicursor: a cursor placed at the beginning for every line of region
* C-x t u 		move current line up (too much like C-x C-t)  
* C-x t d		move current line down
* s-x k			kill buffer and delete related file
* s-x r			rename buffer an related file
* s-t 			toggle orientation of two windows
* s-s			swap position of two windows

## Standard Keystrokes:

* C-f			one caracter forward  
* C-b			one caracter backward  
* C-n			one line down  
* C-p			one line up  
* C-a			go at the beginning of line (not the beginning of indentation)  
* C-e			go at the end of the line  
* M-r			go at the end/center/beginning of the window with the cursor
* C-c spc		jump to word
* C-x spc		jump back

* M-f			one word forward  
* M-b			one word backward  
* M-a			go at the beginning of paragraph  
* M-e			go at the end of paragraph  
* M-m			go at the beginning of the text in this line (beginning of the indentation)
* C-M-n			move forward over a parenthetical group  
* C-M-p			move backward over a parenthetical group 
* C-M-u			move up in parenthesis structure  
* C-M-d 		move down in parenthesis structure  
     			  
* C-s			search forward
* C-r 			search backward  
* C-t		        transpose one caracter  
* C-x C-t 		transpose one line  
* M-t 			transpose one word  
* M-x replace-string	replace all string1 following the pointer with string2   
  
* C-v 			scroll one page down  
* M-v			scroll one page up  
* C-M-v			scroll the not selected window  
* M-<			go to the top of the text  
* M->		      	go to the bottom of the text  
  
* M-u			uppercase until the end of the word  
* M-c			uppercase the caracter and go at the end of the word  
  
* C-space		set a mark  
* C-u C-space		go to mark  
* M-h			select the paragraph  
* C-w			cut selected area between pointer and mark  
* M-w 			copy selected area between pointer and mark  
* C-k			kill one line of text  
* M-k			kill one paragraph of text  
* C-y			paste killed text  
* M-y			choose what paste from the old killed text  
* M-z caracter		kill text until caracter  
  
* C-x (			begin a new macro  
* C-x )			end the beginned macro  
* C-e 			execute last custom macro  
* C-x C-k n		rename last custom macro  
* M-x nameMacro		start custom macro  
* M-x insert-kbd-macro	paste emacs-lisp code of a macro   
  
* C-x 0			close the selected window  
* C-x 1			close all the not selected windows  
* C-x 2 		create a new window under the selected one  
* C-x 3			create a new window near the selected one  
  
* C-x k			kill a buffer  
* C-x s			save all buffers  
* C-x C-s	     	save current buffer  
  
* C-z 			suspend emacs, recover session with fg or %emacs  
  
* M-number		scale the following command  
  
* C-h c keystrokes	really little description of the command  
* C-h k command		description of the command  
* C-h m			information about current editing mode (Fundamental/Text)  
  
* M-x auto-fill	enable auto-fill mode  
* C-x f number	        set fill column to number for auto fill text mode  
* M-q 			fill paragraph  

* C-x i			insert file
* M-; 			comment the end of the line

## Custom manuals
* Bash manual