;; auto save in specific non-polluting directory in .emacs.d?
(use-package no-littering)
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

(use-package undo-tree
  :config
  (setq undo-limit 1000000)
  (setq undo-strong-limit 10000000)
  (setq undo-limit 80000000)
  :hook (list (prog-mode . undo-tree-mode)
              (text-mode . undo-tree-mode)))

;; Enable suggestions for available keystrokes
(use-package which-key
  :diminish which-key-mode
  :config (which-key-mode))

;; Enhance i-search performances
(use-package ivy
  :diminish ivy-mode
  :bind ("C-s" . swiper-isearch)
  ("C-r" . swiper-isearch-backward)
  :config (ivy-mode 1))

;; Improve switch buffer research
(use-package counsel
  :diminish counsel-mode
  :bind (("C-x b" . 'counsel-switch-buffer)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only) ; HERE (a che serve?)
  :config (counsel-mode 1))

(global-display-line-numbers-mode t)
;; Highlight cursor's line
(global-hl-line-mode t)

;; Auto refresh buffers
(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)

;; Disable system's bell
(setq visible-bell 1)

;; Disable startup screen
(setq inhibit-startup-screen t)

;; Enable narrowing and widing
(put 'narrow-to-region 'disabled nil)

;; Remove beckup file
(setq make-backup-files nil)

;; Rebind Alt as Meta
(setq x-alt-keysym 'meta)

;; Turn off mouse interface
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'tooltip-mode) (tooltip-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
;;(set-fringe-mode 10)        ; Give some breathing room (HERE)

;; Set font height 
(set-face-attribute 'default nil :height 150 :width 'extra-condensed)

;; Configure mode line
(setq-default
 mode-line-format
 (list " " 'mode-line-modified                         ;; The "**" at the beginning
       "-- " 'mode-line-buffer-identification         ;; Buffer file name
       "" 'mode-line-position-column-format                ;; %File and line number
       "  " 'mode-line-modes                            ;; Major and minor modes in effect
       "  Time: " '(:eval (format-time-string "%I.%M" (current-time)))
       "   " '(:eval (battery-status))
        '(vc-mode vc-mode)))

(defun swap-windows ()
  "Swap two windows"
  (interactive)
  (cond ((not (> (count-windows)1))
         (message "You can't swap a single window!"))
        (t
         (setq i 1)
         (setq numWindows (count-windows))
         (while  (< i numWindows)
           (let* (
                  (w1 (elt (window-list) i))
                  (w2 (elt (window-list) (+ (% i numWindows) 1)))

                  (b1 (window-buffer w1))
                  (b2 (window-buffer w2))

                  (s1 (window-start w1))
                  (s2 (window-start w2))
                  )
             (set-window-buffer w1  b2)
             (set-window-buffer w2 b1)
             (set-window-start w1 s2)
             (set-window-start w2 s1)
             (setq i (1+ i)))))))

(defun delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(defun move-line-down ()
  "Move the current line one line down, keep the cursor on it"
  (interactive)
  (let ((col (current-column)))
    (save-excursion
      (forward-line)
      (transpose-lines 1))
    (forward-line)
    (move-to-column col)))

(defun move-line-up ()
  "Move the current line one line up, keep the cursor on it"
  (interactive)
  (transpose-lines 1)
  (previous-line 2))

(defun toggle-window-split ()
  "Toggle orientation of two windows"
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(defun save-macro (name)
  "Save a macro. 
Take a name as argument and save the last defined macro under 
this name at the end of your init.el"
  (interactive "SName of the macro: ")
  (kmacro-name-last-macro name)
  (find-file user-init-file)
  (goto-char (point-max))
  (newline)
  (insert-kbd-macro name)
  (newline)
;;Autosaving and quit buffer
  (save-buffer)
  (kill-buffer))
;;If you prefer to check the modified file before saving
;;  (switch-to-buffer nil))


(defun vim-o ()
  "Easy implementation of the o vim command."
  (interactive)
  (move-beginning-of-line nil)
  (newline)
  (previous-line)
  (indent-for-tab-command))

(defun vim-O ()
  "Easy implementation of the O vim command."
  (interactive)
  (move-end-of-line nil)
  (newline)
  (indent-for-tab-command))

  ;; Rows movements
  (global-set-key (kbd "C-x t u") 'move-line-up)
  (global-set-key (kbd "C-x t d") 'move-line-down)

  ;; Window's layout management
  (global-set-key (kbd "C-x w t") 'toggle-window-split)
  (global-set-key (kbd "C-x w s") 'swap-windows)

  ;; Buffer's related files 
  (global-set-key (kbd "C-x x r") 'rename-current-buffer-file)
  (global-set-key (kbd "C-x x k") 'delete-current-buffer-file)
  ;; insert file C-x x i

  ;; Replace string
  (global-set-key (kbd "C-x r s") 'replace-string)

  ;; Join lines
  (global-set-key (kbd "M-j") (lambda () (interactive) (join-line -1)))

  ;; Vim-f
  ;(global-set-key (kbd "C-c f") 'iy-go-to-char)
  ;(global-set-key (kbd "C-c b") 'iy-go-to-char-backward)

  ;; Vim-O
  (global-set-key (kbd "M-o") 'vim-O)

  ;; Vim-o
  (global-set-key (kbd "C-o") 'vim-o)

  ;; Open-line (ex standard C-o)
  (global-set-key (kbd "M-RET") 'open-line)

  ;; Compile
  (add-hook 'prog-mode-hook '(lambda ()
     (local-set-key (kbd "C-c c") 'compile)))

  ;; Recompile
  (add-hook 'prog-mode-hook '(lambda ()
     (local-set-key (kbd "C-c r") 'recompile)))

;; Bookmarks rebinds
(global-set-key (kbd "C-c b s") 'bookmark-set-no-overwrite)
(global-set-key (kbd "C-c b a") 'bookmark-set-no-overwrite) ; same
(global-set-key (kbd "C-c b x s") 'bookmark-save)
(global-set-key (kbd "C-c b j") 'bookmark-jump)
(global-set-key (kbd "C-c b b") 'bookmark-jump) ; same
(global-set-key (kbd "C-c b d") 'bookmark-delete)
(global-set-key (kbd "C-c b k") 'bookmark-delete) ; same
(global-set-key (kbd "C-c b l") 'list-bookmarks)
(global-set-key (kbd "C-c b f l") 'bookmark-load)
(global-set-key (kbd "C-c b f s") 'bookmark-write)

;; Python mode
(defun eval-section (evaluation-function)
  (interactive)
  "Evaluate the current region delimited by #% tags"
   (save-excursion (search-backward "#%")
                   (push-mark (point))
                   (activate-mark)
                   (forward-char 2)
                   (search-forward "#%")
                   (eval evaluation-function)
                   (pop-mark)))

(defun python-eval-section ()
  (interactive)
  (eval-section '(python-shell-send-statement)))

(global-set-key (kbd "C-c z") 'run-python)
(global-set-key (kbd "C-c f") 'python-shell-send-buffer)
(global-set-key (kbd "C-c e") 'python-shell-send-statement)

;; Debugger mode
(global-set-key (kbd "C-c d b") 'gud-break)
(global-set-key (kbd "C-c d d") 'gud-remove)
(global-set-key (kbd "C-c d s") 'gud-step)
(global-set-key (kbd "C-c d n") 'gud-next)
(global-set-key (kbd "C-c d c") 'gud-cont)
(global-set-key (kbd "C-c d r") 'gud-finish)
;; Configure python debugger mode
(setq gud-pdb-command '(concat '"python3 -m pdb " buffer-file-name))
(add-hook 'python-mode-hook '(lambda () (setq gud-pdb-command-name (eval gud-pdb-command))))

;(set (make-local-variable 'gud-pdb-command-name)
;     (concat "python3 " (buffer-file-name)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(tsdh-dark))
 '(warning-suppress-log-types '((comp)))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
