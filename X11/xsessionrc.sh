# If a Desktop Environment is used, manually load the bash profile
if [ -r ~/.bash_profile ]; then
  . ~/.bash_profile
fi

stalonetray &
cbatticon BAT1 &
cbatticon BAT0 &
nm-applet &
volumeicon &
ivpn-ui &
redshift-gtk -t 5500K:2000K -l 45.116177:7.742615 &
kdeconnect-indicator &
sleep 60 && megasync &
feh --bg-scale ~/Pictures/Wallpapers/Islanda.JPG &

# If Xmonad is the used DE, launch some default programs
#cmds=("stalonetray &" "cbatticon BAT1 &" "ivpn-ui &" "nm-applet &" "volumeicon &" \
#      "redshift-gtk -t 5500K:2000K -l 45.116177:7.742615 &" \
#      "feh --bg-scale ~/Pictures/Wallpapers/Islanda.JPG &" )
#
#for i in ${!cmds[@]}; do
#    currentCMD=${cmds[$i]}
#    set -- $currentCMD
#    currentPREFIX=$1
#    # Detect possible old processes running
#    alreadyRunning=`ps aux | grep $currentPREFIX | grep -v grep`
#    # Kill the old processes
#    if [ -n "$alreadyRunning" ] ; then
##      killall $currentPREFIX
#      echo "killall $currentPREFIX" >> killall.log
#    fi
#    # Start the new processes
#    eval $currentCMD
#done

