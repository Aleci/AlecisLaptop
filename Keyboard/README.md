# ITalian keyboard for Emacs and IT Engineers (ITE)
## Why i use ite layout
I am italian, so i learned to type with the it layout.  
When i began to program i discovered that the symbols useful to code were tricky to get from an it keyboard.  
So i learned to type with the us layout.  
But when i have to write text in italian i still need the symbols of the it layout.  
Swapping between 2 different layouts really confuse me and drammatically decrease my typing rate.  
The ite layout fixs all this problems, because basically is an us keyboard with italian symbols.  

## How ite works
Every key has 4 levels (of course not the special keys) and every level can give a different symbol.  
The modifiers used in this keymap are Shift and AltGr.      key = [ none, Shift, AltGr, AltGr+Shift ]    
The italian symbols (the vowel with accents) are almost all in the same place of the it layout but in the third level.  
To use them faster i placed a left AltGr near the Shift (you need a gb physical keyboard to be sure to have it).  
Other keys have mathematic symbols in the third and forth level.  
I use GNU Emacs so there are two Alt key near the space key, instead of one.  
This layout should work well with a generic 105 key keyboard, but is optimized for lenovo Thinkpad X200T gb keyboard.  

## Modify ite's symbols
If you want to modify the ite keyboard for your needs you have just to write the unicode (UXXXX) of your symbol in the "form" of the key.    
The form for the key a is something like  { [            a,              A,       U03B1,         U2200 ]	};  
You can also find the alias defined in the /usr/include/X11/keysymdef.h library that binds the unicode to an english word, but is boring so i didn't.  
If you are using GNU Emacs and you want to know the meanig of an unicode just type: C-x 8 RET and then insert the code without the U at the beginning.  

## Modify ite's Modifier
If you want to change the key of AltGr you have to find the name of the new AltGr and put it in this "form" :   
    `key <MENU> {   
    type[Group1]="ONE_LEVEL",  
    symbols[Group1] = [ ISO_Level3_Shift ]  
    };`  
Here i set the menu key as a AltGr.  

You can find the most common names of the keys in the figure 2 of this guide and other useful references: http://www.charvolant.org/doug/xkb/html/node5.html  

## Install ite's symbols
Here i did a really bad job because i don't know how to install properly my keymap (and i really dont care) so i replaced the estonian keyboard (ee).  
This is the rude way that works and is not that awful (ee=Engineer that use Emacs).  
`# cp ite /usr/share/X11/xkb/symbols/ee`  

To load one time use this.  
` $ setxkbmap ee `   
If you want to set as default in Trisquel run:
` # dpkg-reconfigure xkb-data ` 
and then choose the Estonian keyboard running:
` $ ibus-setup `  

## Bulk keymap installations without root privileges
Is possible to export the previous installation as a bulk configuration file using:  
` $ xkbcomp $DISPLAY dump.txt `  
To load without root privileges the dump you have to run:  
` $ xkbcomp dump.txt $DISPLAY `  

## Nice documentation
https://stackoverflow.com/questions/28527938/xkb-configuration-for-emacs
https://stackoverflow.com/questions/21845209/remap-ctrl-alt-super-keys-in-ubuntu-13-10

# Trackpad configs
It's possible to configure speed and inertia of the trackpoint with the following command:
`# cp 12-trackpoint.rules /etc/udev/rules.d/12-trackpoint.rules`  
