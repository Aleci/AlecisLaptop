#!/bin/bash

#===============================
REPO_PATH=$HOME/Git/AlecisLaptop
#===============================


# Add the user to group sudo and run this script as standard user after a reboot
# su -
# usermod -aG sudo myUser

# Detect hybrid system
if [ "uname -v | grep Debian" ] ; then
    HYBRID=true; 
else
    HYBRID=false;
fi

# Configure Git
git config --global user.name "Alessandro Cavalli"
git config --global user.email "aleci@inventati.org"
git config --global credential.helper "cache --timeout=7200"
echo "ATTENTION: Personal git configuration applied:"
git config --global --list

# Install Guix
if $HYBRID ; then
  cd /tmp
  wget https://sv.gnu.org/people/viewgpg.php?user_id=15145 -qO - | gpg --import 
  wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
  chmod +x guix-install.sh
  sudo ./guix-install.sh
fi

# Update root and user instance of Guix
sudo guix pull
sudo guix package -u
#
guix pull
guix package -u

# Configure locale on hybrid system
if $HYBRID ; then
    sudo guix install glibc-locales
    guix install glibc-locales
fi

# Configure home dotfiles
sudo guix home reconfigure $REPO_PATH/Guix/Home.scm
guix home reconfigure $REPO_PATH/Guix/Home.scm

# Install fundamental extra profiles
cd $REPO_PATH/Guix/Profiles
for i in "Emacs" "Xmonad" "Utils"; do
    . installProfile.sh $i
done

# Load Guix environment
. $HOME/.bash_profile

# Install fallback packages for extra profiles
if $HYBRID ; then
    sudo apt install curl
    # Add IVPN key and repository
    curl -fsSL https://repo.ivpn.net/stable/debian/generic.gpg | gpg --dearmor > ~/ivpn-archive-keyring.gpg
    sudo mv ~/ivpn-archive-keyring.gpg /usr/share/keyrings/ivpn-archive-keyring.gpg
    curl -fsSL https://repo.ivpn.net/stable/debian/generic.list | sudo tee /etc/apt/sources.list.d/ivpn.list
    # Install packages
    sudo apt update -y
    sudo apt install xmonad i3lock ghc ivpn-ui redshift-gtk -y
    # Post installation settings
    sudo ln -s /opt/ivpn/ui/bin/ivpn-ui /bin/
fi

cabal update
cabal install xmonad --lib xmonad-contrib --lib

# Create and enable custom services in hybrid systems
if $HYBRID ; then
    # Add services
    sudo cp $REPO_PATH/Xmonad/powerStates.service /etc/systemd/system/
    # Enable services
    sudo systemctl enable powerStates.service
fi

# Add contrib and non-free repository and install firmware-iwlwifi card driver and firmware-linux package
