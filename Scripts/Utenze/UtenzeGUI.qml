import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import org.julialang

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello, World")
    GridLayout {
	anchors.fill: parent
	
	columns: 4
	rows: 3
	
	Rectangle {
            Layout.column: 0
            Layout.columnSpan: 1
            Layout.row: 0
            Layout.rowSpan: 1
            Layout.fillHeight: true
            Layout.fillWidth: true
	    Layout.preferredWidth: Layout.columnSpan
            Layout.preferredHeight: Layout.rowSpan
            color: "red"
	}

	Rectangle {
            Layout.column: 1
            Layout.columnSpan: 3
            Layout.row: 0
            Layout.rowSpan: 3
            Layout.fillHeight: true
            Layout.fillWidth: true
	    Layout.preferredWidth: Layout.columnSpan
            Layout.preferredHeight: Layout.rowSpan
            color: "green"
	    
	    JuliaDisplay {
		id: jd
		width: 1100
		height: 400
		anchors.centerIn: parent
		Component.onCompleted: {
		    Julia.plotPSV(jd,jd.width,jd.height)
		}
	    }


	}

	Rectangle {
            Layout.column: 0
            Layout.columnSpan: 1
            Layout.row: 1
            Layout.rowSpan: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
	    Layout.preferredWidth: Layout.columnSpan
            Layout.preferredHeight: Layout.rowSpan
            color: "blue"
	    Rectangle {
		color: "yellow"
		height: parent.height*0.9
		width: 203
		anchors.centerIn: parent
		TableView {
		    id: tableView
		    anchors.fill: parent
		    anchors.centerIn: parent
		    
		    columnSpacing: 3
		    rowSpacing: 2
		    
		    model: dataTable
		    
		    delegate: Rectangle {
			implicitWidth: 100
			implicitHeight: 25
			
			Text {
			    anchors.centerIn: parent
			    text: display
			}
		    }
		}
		Rectangle {
		    id: header
		    color: "magenta"
		    anchors.top: tableView.top
		    height: 30
		    width: tableView.width
		}
	    }

	}
    }
}	
