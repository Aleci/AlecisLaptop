# Gui QML
# Database SQLite
#import Pkg; Pkg.add("Measures")
using TableScraper, DataFrames, Plots, SQLite, QML, Printf, Measures
gr(dpi=300)

function scaricaTabellaDaSito(df,url)
# La struttura della tabella deve essere gestita esternamente con il df in input
    st = scrape_tables(url)
    for riga in first(st).rows
        push!(df,riga)
    end
end

function formattaData(df)
    df[!,:anno]=parse.(Int64,replace.(df.mese,r"[a-zA-Z ]*"=>""))
    mesi=["Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"]
    for n in 1:length(mesi)
        df[!,:mese]=replace.(df.mese,Regex(mesi[n]*" [0-9]{4}")=>n)
    end
    df[!,:mese]=parse.(Int64,df.mese)
    return df
end

function scaricaPsv()
    # Indice psv a2a
    urlPsv="https://www.a2a.it/assistenza/tutela-cliente/indici/indice-psv"
    psv=DataFrame(mese=[],indice=[])
    scaricaTabellaDaSito(psv,urlPsv)
    formattaData(psv)
    return psv
end

function scaricaPun()
    # Indice pun gme a2a
    urlPun="https://www.a2a.it/assistenza/tutela-cliente/indici/indice-pun"
    pun=DataFrame(mese=[],f123=[],f1=[],f2=[],f3=[],f23=[])
    scaricaTabellaDaSito(pun,urlPun)
    formattaData(pun)
    return pun
end

function aggiornaPun()
    pun=scaricaPun()
    punStorico=DBInterface.execute(conn,"SELECT * FROM pun") |> DataFrame
    pJoin=leftjoin(pun,punStorico,on=[:mese,:anno],makeunique=true)
    nuoviValori=filter(riga -> ismissing(riga.id),pJoin)
    indici=[:f123,:f1,:f2,:f3,:f23]
    for col in indici
        nuoviValori[!,col]=replace.(nuoviValori[!,col],","=>".")
    end
    for riga in eachrow(nuoviValori)
        DBInterface.execute(conn,"INSERT INTO pun(mese,anno,F123,F1,F2,F3,F23) VALUES ($(riga.mese),$(riga.anno),$(riga.f123),$(riga.f1),$(riga.f2),$(riga.f3),$(riga.f23))")
    end
end

function aggiornaPsv()
    psv=scaricaPsv()
    psvStorico=DBInterface.execute(conn,"SELECT * FROM psv") |> DataFrame
    pJoin=leftjoin(psv,psvStorico,on=[:mese,:anno],makeunique=true)
    nuoviValori=filter(riga -> ismissing(riga.id),pJoin)
    nuoviValori[!,:indice]=replace.(nuoviValori.indice,","=>".")
    for riga in eachrow(nuoviValori)
        DBInterface.execute(conn,"INSERT INTO psv(mese,anno,indice) VALUES ($(riga.mese),$(riga.anno),$(riga.indice))")
    end
end

function meseanno2data(df)
    transform!(df,AsTable([:mese,:anno]) => ByRow(riga -> @sprintf "%02d/%4d" riga.mese riga.anno) => :data)
end

function indicePsv()
    psv=DBInterface.execute(conn,"SELECT * FROM psv ORDER BY anno,mese ASC") |> DataFrame
    meseanno2data(psv)
end

function indicePun()
    pun=DBInterface.execute(conn,"SELECT * FROM pun ORDER BY anno,mese ASC") |> DataFrame
    meseanno2data(pun)
end

conn=SQLite.DB("UtenzeDB.sqlite3")
aggiornaPsv()
aggiornaPun()

psv=indicePsv()#Gas
pun=indicePun()#Luce


function plotPSV(julia_display::JuliaDisplay, width::Float64, height::Float64)
    p=plot(psv.data,psv.indice,xrot=30,show=false,
           size=(Int64(round(width)),Int64(round(height))),
           bottom_margin=4mm)
    display(julia_display,p)
    return nothing
end

@qmlfunction plotPSV

# Add data to Qt Table
dataTableMDL=JuliaItemModel(select(psv,[:data,:indice]))

# Round data in qt table
#setgetter!(dataTableMDL, x -> string(round(x;digits=3)), QML.DisplayRole)

# Add headers in qt table
function getheader(df, row_or_col, orientation, role)
    if orientation == QML.Horizontal
        return string(names(df))
    end
    return "Unknown"
end
setheadergetter!(dataTableMDL, getheader)

loadqml("UtenzeGUI.qml",dataTable=dataTableMDL)
exec()

## Mesi
##giugno
##luglio
##agosto
##settembre
##ottobre
#
## Condizioni economiche
#ce=[22.88;23.26;42.67;24.06;31.07]
## pun
#pun=[0.10317;0.112320;0.12844;0.11713;0.116690]
## kilowattora
#kwh=[114;110;213;121;119]
## costo contratto
#cc= kwh .* (pun .+ 0.022) .+ (69.17/12)
#
#costoVero=zip(kwh,ce) |> collect
#costoContratto=zip(kwh,cc) |> collect
#
#tv=sort(costoVero)
#tc=sort(costoContratto)
#x=map(x->x[1],tv)
#y=map(x->x[2],tv)
#yc=map(x->x[2],tc)
#A=[x ones(5,1)]
#b=y
#bestFit=inv(A'*A)*A' *b
#yF=bestFit[1] .* x .+ bestFit[2]
#
#p=plot(tv,label="vero")
#p2=plot!(p,x,yF,label="fittato")
#p3=plot!(p2,tc,label="contratto") |> display
#
#e=y-yc
#sum(e)
#
