* TODOLIST
Salvare in file separato la data di riconciliazione.
** TODO Convertire automaticamente in xlsx (ora è manuale senza make) (script facile da lanciare con doppioclick?)
** TODO Aggiungere Tag a Dettagli transazione (gestire missing)
** TODO Cancellare file di input (e xlsx) da Downloads
* Procedura aggiornamento:
** prelievo => cash
** cancellare bonifici periodici entranti in unicredit (visti da unicredit)
** segnare mediche
** cambiare transfer account in
*** DB  (somma totale)
**** Cheching <- entrano qui (liquido pulito)
**** Risparmi (liquido fermato)
* Vecchia procedura
1) Vedere data ultima riconciliazione
2) Scaricare csv dai siti delle banche delle transazioni dalla ultima riconciliazione ad oggi
3) Importare i csv verso l'account nomeBanca/Checking
4) Sostituire imbalace-eur con l'account giusto e razionalizzare
5) Inserire scontrini spese cash
6) Effettuare riconciliazione (da fare lo stesso giorno altrimenti modificare il file con la data di riconciliazione prima del pull successivo)
* Unicredit configs
File->Import->Import Transactions from CSV...
** Settings
(Skip header) (Modifica formato data in d-m-y) (Modifica formato valuta)
- None
- None
- Date
- Description
- Withdrawal
* DB Contocarta configs
File->Import->Import Transactions from CSV...
** Settings
(Skip header) (Modifica formato data in d-m-y)
- Date
- None
- Withdrawal
- Deposit
- None
- None
- Dettagli
- Transfer Account
- Memo
* Importer config
File->Import->Import Transactions from CSV...
** Settings
- Semicolon
- Leading lines to skip 1
- Account = Importazione (generic Expense account)
** CSV Header
- Data = Date
- Descrizione = Description
- Prelievi = Withdrawal
- Conto = Transfer Account
