#import Pkg; Pkg.add("CSV")

using DataFrames, XLSX, Dates, CSV

function mergemissing(x,y)
    if(ismissing(x) && !ismissing(y))
        y
    elseif(!ismissing(x) && ismissing(y))
        x
    else
        error("Errore: dati non validi")
    end
end

# Percorsi dei files in input
geniusPath="/home/ale/Downloads/Elenco_Movimenti.csv"
contocartaPath="/home/ale/Downloads/estrattocontoitalia.xls"

# Converti xls in xlsx con libreoffice ed usa il nuovo file nella procedura
#  PER ORA FATTO DA MAKEFILE
#str_cmd=string("libreoffice --convert-to xlsx ", contocartaPath, " --headless")
#str_cmd_v=split(str_cmd); convert_cmd=`$(str_cmd_v)`; run(convert_cmd)
contocartaPath=string(contocartaPath,"x")

# Leggi le liste delle transazioni appena scaricate
# Data Registrazione;Ora operazione;Data valuta;Descrizione;Importo;
genius=CSV.read(geniusPath,DataFrame,delim=";",dateformat="d/m/y",header=false,skipto=2)
# Data contabile; Valuta; Dare; Avere; Divisa; Operazione; Dettagli; Categoria; Tag
contocarta=DataFrame(XLSX.readtable(contocartaPath, "estrattocontoitalia"))

# Carica la data dell'ultima importazione
data_importazione = CSV.read("Data.csv",DataFrame).Data_Importazione[1]

# Elimina le transazioni con data contabile precedente alla data dell'ultima importazione
deleteat!(genius, findall(x-> x<data_importazione, genius.Column1))
deleteat!(contocarta, findall(x-> x<data_importazione, contocarta."Data contabile"))

# Accorpa prelievi e versamenti contocarta in un'unica colonna (Positivo entra nel mio conto e negativo esce dal mio conto)
colonnaPrelievi=combine(contocarta,[:Dare,:Avere]=>ByRow((x,y)->mergemissing(x,y))=>:Prelievi)
contocarta.Prelievi=colonnaPrelievi.Prelievi

# Accorpa dettagli e tipo operazione contocarta in un'unica colonna e rimuovi gli a capo per compatibilità con csv
contocarta.Descrizione = contocarta.Dettagli .* "\nOperazione: " .* contocarta.Operazione #.* "\\nTag: " .* contocarta.Tag
contocarta.Descrizione = map( x->replace(x, "\n" => "| "), contocarta.Descrizione)

# Sostituisci la virgola con il punto nei prelievi unicredit
genius.Column5=map( x->replace(x, "," => "."), genius.Column5)

# Costruzione tabella standard
# Date (contabile), description, withdrawal, dettagli, transfer account (Not_imported), memo
genius_std=select(genius,:Column1 => "Data",:Column4 => "Descrizione",:Column5 => "Prelievi")
genius_std[!, :Conto] .= "Assets:GeniusCard" 
contocarta_std=select(contocarta,:"Data contabile" => "Data",:Descrizione => "Descrizione",:Prelievi => "Prelievi")
contocarta_std[!, :Conto] .= "Assets:ContoCarta"

# Unisci le due tabelle (ESEMPIO)
tabella_std=vcat(genius_std, contocarta_std, cols = :union)

# Esporta la tabella standard in csv
CSV.write("TransazioniSTD.csv", tabella_std, delim=";")

# Aggiorna la data dell'ultima importazione ad oggi
CSV.write("Data.csv", DataFrame(Data_Importazione = now())) #Date(2023,12,23)))   < ----------- debug!
