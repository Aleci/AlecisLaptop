#!/bin/bash

# 1) From firefox visit the scribd page
#    Go to options (the three bars) and select "Save page as"
#    Use the name scribd.html to save the page.
#    Open a terminal in the same directory of the downloaded page.

# 2) Grep all the jsonp uri from the html an save them in a temporary file
cat scribd.html | grep https://html.scribdassets.com | grep -Po '"\K[^"]*' | grep jsonp > jsonpFile.txt

# 3) Change the jsonp uri using the pattern in order to unlock the images
cat jsonpFile.txt | sed 's/pages/images/' | sed 's/jsonp/jpg/' > jpgFile.txt

# 4) Download all the images in a new folder
mkdir scribd_images
cd scribd_images
wget -i ../jpgFile.txt

# 5) Install imagemagic and edit the policy commenting the policy on PDF in this way
#   File: /etc/ImageMagick-7/policy.xml
#   <!-- <policy domain="coder" rights="none" pattern="PDF" /> -->

# 6) Convert all the images in pdf and save their names in a variable
for i in $(seq 1 `ls | wc -l`); do
    convert $i-* $i.pdf 
    VAR="$VAR $i.pdf" 
done

# 7) Concatenate all the pdf in a single document
pdftk $VAR cat output document.pdf

# 8) Restore PDF policy in /etc/ImageMagick-7/policy.xml











### FALLIMENTARY PROCEDURE 1 ###

# 1) Visit the scripd website and press F12
#    right click on the html tag and choose "Expand All"
#    wait for the job completion
#    Go to options (the three bars) and select "Save page as" (NON SALVA LE MODIFICHE, CAPIRE COME COPIARE DALL'EDITOR DI FIREFOX)
#    use the name scribd.html
#    open a terminal
#    just grep and wget the images

### FALLIMENTARY PROCEDURE 2 ###

## 0) Provide as input of this command the scribd uri of the pdf
## 1) Save the html page and save in a temporary file 
##mkdir scribd
##cd scribd
##wget $@ -O file.html
## 2) Grep all the jsonp uri from the html an save them in a temporary file
#cat file.html | grep https://html.scribdassets.com | grep -Po '"\K[^"]*' > jsonpFile.txt
## 3) Download all the jsonp files in a temporary folder
#mkdir jsonpFiles
#cd jsonpFiles
#wget -i ../jsonpFile.txt
## 4) Join all the jsonp files and grep all the jpg uri, modify them with the correct domain and save them in a temporary file
#cat * | grep -Poa 'http://html.scribd.com\K[^"]*' | sed 's/^/http:\/\/html.scribdassets.com/' | sed 's/.$//' > jpgFile.txt
## 5) Download all the jpg files
#mkdir jpgFiles
#cd jpgFiles
#wget -i ../jpgFile.txt
## 6) Some nice exultance
#echo "Done... Daje Roma!"
## 7) The resulting pages scraped from the website will be in scribd/jsonpFiles/jpgFiles
