# Dati
RAL=28   *1000;
tfr=RAL / 13.5 - 0.05*RAL; # Annuo
maxDeduzioni=5164.57 # Annui
pAzienda=1.55 /100;
pBase=0.55 /100;

contribuzioneFissa=(pAzienda+pBase)*RAL
pVolontario=(maxDeduzioni - contribuzioneFissa)/RAL

contribuzione=contribuzioneFissa+pVolontario*RAL
contribuzioneAzienda=pAzienda*RAL
contribuzioneDipendente=(pBase+pVolontario)*RAL
contribuzioneDipendenteMensile=contribuzioneDipendente/12
contribuzioneTotale=tfr+contribuzione

println("Percentuale contributo volontario: ", pVolontario)
println("Contribuzione dipendente mensile: ",contribuzioneDipendenteMensile)
println("Contribuzione dipendente: ",contribuzioneDipendente)
println("Contribuzione azienda: ",contribuzioneAzienda)
println("Contribuzione totale: ",contribuzione)
println("Contribuzione + TFR: ",contribuzioneTotale)
