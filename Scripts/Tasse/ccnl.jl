using Plots
default(show=true); default(minorgrid=true); default(dpi=300)

ral=28000
ore=1:200;


# Commercio
# Stipendio in 14 mensilità
# 26 giorni di ferie (4 settimane da 6 giorni + 2 giorni)
# Straordinari a partire da 15% per max 250 ore annue
# Permessi pagati 72 ore annue (max 4 ore al giorno richieste 10 giorni prima?)
# Fondo Est: assistenza sanitaria integrativa

paga_oraria_c=ral/14/168
paga_orario_straordinario_c=paga_oraria_m*1.15
straordinario_c=ore.*paga_orario_straordinario_c
plot(ore,straordinario_c,label="Commercio",title="Retribuzione straordinari",legend=:topleft, xlabel="Ore straordinario", ylabel="€")

# Metalmeccanico
# Stipendio in 13 mensilità
# 24 ore formazione a triennio
# 20 giorni di ferie (4 settimane da 5 giorni)
# Straordinari a partire da 25% per max 200 ore annue
# Permessi pagati 104 ore annue (max 4 ore al giorno richieste 10 giorni prima)
# Welfare obbligatorio
# Fondo Metasalute: assistenza sanitaria integrativa
# Cometa opzionale

paga_oraria_m=ral/13/168
paga_orario_straordinario_m=paga_oraria_m*1.25
straordinario_m=ore.*paga_orario_straordinario_m
plot!(ore,straordinario_m,label="Metalmeccanico")

plot(ore,straordinario_m .- straordinario_c,title="Differenza di retribuzione straordinari", xlabel="Ore straordinario", ylabel="€")
