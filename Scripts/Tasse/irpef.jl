using Plots

################
# Dati in euro #
################
ral_max=38               *1000;
ral_min=27               *1000;

ral=36                   *1000;
mensilità=13                  ;

rimborsiTrasferte=0           ; # Mensili
deduzioni=0                   ; # Annuali
detrazioni=0                  ; # Annuali

##################
# Parametri 2024 #
##################
scaglioni=[28,50]        *1000; 
aliquote=[23,35,43]       /100;
#scaglioni=[];
#aliquote=[0]       /100;
#
aliquotaINPS=(9.19+0.30)   /100;
#aliquotaINPS=5.84 /100;
#
addizionaliRegionali=1.5  /100;


##############
# Detrazioni #
##############
function detrazione_dipendente(ral)
    # Relativo al 2024 (articolo 13 del testo unico del TUIR, decreto legislativo n. 216/2023)
    if (ral <= 15000)
        detrazione=1955
    elseif (ral <= 28000)
        detrazione=1910+1190*(28000-ral)/13000
    elseif (ral <= 50000)
        detrazione=1.910*(50000-ral)/22.000
    else
        detrazione=0
    end
    # il bonus irpef 2024 non e' considerato perche' e' applicabile a ral < 15000
end
detrazioni_tot=detrazioni+detrazione_dipendente(ral)


##############
# Previdenza #
##############
function esonero_contributivo_mensile(ral)
    # Relativo al 2024
    if (ral <= 25000) 
        esonero=0.07*ral
    elseif (ral <= 35000)
        esonero=0.06*ral
    else
        esonero=0
    end
    esonero=esonero/mensilità
end
esonero_contributivo_annuo(ral)=12*esonero_contributivo_mensile(ral/mensilità)
contributi(imponibileINPS)=aliquotaINPS * round(imponibileINPS)


#########
# IRPEF #
#########
function irpef_scaglioni(imponibile, scaglioni, aliquote) 
    # Controlla validità dati
    if ((!(length(scaglioni)==length(aliquote)-1)) || !issorted(scaglioni) || !issorted(aliquote))
        error("Errore: aliquote o scaglioni non validi")    
    end
    irpef_ricorsiva(imponibile,scaglioni,aliquote)
end
function irpef_ricorsiva(imponibile,scaglioni,aliquote)
    # Caso base: Rientro nello scaglione corrente
    if ((scaglioni == []) || (imponibile - scaglioni[1] < 0))  
        aliquote[1]*imponibile;
    # Passo ricorsivo
    else 
        aliquote[1]*scaglioni[1] + irpef_ricorsiva(imponibile-scaglioni[1], scaglioni[2:end], aliquote[2:end])
    end
end
irpef(imponibileIRPEF)=irpef_scaglioni(imponibileIRPEF,scaglioni,aliquote)


###############
# Altre tasse #
###############
addizionali(ral)=addizionaliRegionali * ral


############
# Funzioni #
############
function RAN(ral)
    inps=contributi(ral)
    imponibile=ral-inps-deduzioni;
    tasse=irpef(imponibile)-detrazioni_tot;
    ran0=ral-inps-tasse+esonero_contributivo_annuo(ral);
#    ran0-addizionali(ran0);
end


#################
# Test funzioni #
#################

# Stampa dei risultati
println()
println("========= Dati Fiscali =========")
println("Imp. Fisc. mese: ", (ral-contributi(ral))/mensilità+esonero_contributivo_mensile(ral))
println("Imp. lorda mese: ", irpef(ral)/mensilità)
println("Detr. L. dip. prog.: ", detrazione_dipendente(ral)/mensilità)
println("RETR. UT. TFR: ", ral/mensilità)
println("ACC. TFR MESE: ", ral/mensilità^2)
println("CTR. 0.50% TFR: ", ral/mensilità*0.005)
println()
println("========= Dati Previdenziali =========")
println("IMP. PREV. PR.: ", ral/mensilità)
println("CTR PREV. PR.: ", contributi(ral)/mensilità)
println()
println("========= Statistiche =========")
println("Stipendio netto: ", RAN(ral)/mensilità)



## Calcolo ran a partire da ral specifica
ran=RAN(ral);
stipendio=ran/mensilità + rimborsiTrasferte;
stipendioLordo=ral/mensilità + rimborsiTrasferte;
#
## Calcolo ran per intervallo di ral
ralV=ral_min:100:ral_max;
ranV=map(RAN,ralV);
#
## Grafico dei risultati
default(show=false,dpi=300,minorgrid=true)
plot(ralV/1000,ranV/mensilità,label="Netto mensile su $mensilità mensilità",xlabel="RAL [K€]",ylabel="[€]")
plot!([ral]/1000,[ran]/mensilità,markershape=:circle,markercolor=:red, label=:none,linealpha=0)
savefig("StipendioVsRal.png")
