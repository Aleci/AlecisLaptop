#% Workspace initialization
using Debugger
break_on(:error) # @run function containig @bp

#import Pkg; Pkg.add("Printf")
using ActuaryUtilities, Dates, YFinance, PlotlyJS, Statistics, DataFrames, JLD2, DayCounts, Printf

oggi=Dates.today();

# CONVERTIRE QUESTA FUNZIONE IN GENERICA E PRENDERE COME ARGOMENTI f e :fRef in cui salvare i dati
function medianBenchmark(indexDF)
    db=indexDF[3]
    lastIndex=findfirst(ismissing, db.irrv)-1 # prendi la timeserie dei rendimenti a x anni veri
    med=median(db.irrv[1:lastIndex]) # calcola la mediana del rendimento a x anni
    initialVal=db.close[1]  # prendi il valore iniziale dell'investimento
    initialTime=Date(db.timestamp[1]) # prendi l'instante temporale iniziale
# DAI PRECEDENTI DATI CALCOLARE LA TIMESERIE DEGLI INTERVALLI DI INVESTIMENTO
# FUNZIONE DA MAPPARE
#ΔT=yearfrac(initialTime,Date(db.timestamp[2]),DayCounts.ActualActualISDA()) # expressed in years
    ΔTs=map(x->yearfrac(initialTime,Date(x),DayCounts.ActualActualISDA()) , db.timestamp)
# RIVALUTARE IL CAPITALE DEL med PER IL TUTTI I ΔTs
    # trovare formula e mapparla
    ref=map(x->initialVal*(1+med)^x, ΔTs)
    db[!,:medianRef]=ref
    return ref
end

function update_pricesDB(pricesDB)
    updatedPricesDB=[]
    for i in pricesDB # priceDB is a list of tuples (immutable object)
        lastFetch=Dates.yearmonthday(i[3].timestamp[end]+Day(1))
        lastFetchDate=Date(lastFetch[1],lastFetch[2],lastFetch[3])
        if(oggi != lastFetchDate)
            newPrices=get_prices(i[1],interval=i[2],startdt=lastFetchDate,enddt=oggi) |> DataFrame
            updatedPrices=vcat(i[3],newPrices) # append new prices to the existing ones
            updatedPricesDB=vcat(updatedPricesDB,(i[1],i[2],updatedPrices,i[4],i[5]))
        else
            updatedPricesDB=vcat(updatedPricesDB,i)
        end
    end
    save_object("HistoricalPrices.jld2", updatedPricesDB)
    return updatedPricesDB
end

function dualPlot(x1,y1,x2,y2,p_title,xlabel,ylabel,ylabel2)
    trace1=scatter(x=x1,y=y1,yaxis="y2") #,name=indexName (legend entry)
    trace2=scatter(x=x2,y=y2)            #,name="Daily variation" (legend entry)
    lyo=Layout(title=p_title,showlegend=false,
               yaxis_domain=(0, 0.5), yaxis2_domain=(0.5, 1),
               yaxis2_title=ylabel, yaxis_title=ylabel2,
               xaxis_title=xlabel)
    p=plot([trace1,trace2],lyo)
    return p
end

function plotIndex(indexDB)
    indexDF=indexDB[3]; indexName=indexDB[4]; indexCurrency=indexDB[5]
    index=indexDF.close; index_t=indexDF.timestamp
    lastIndex=findfirst(ismissing, indexDF.irrv)-1 # prendi la timeserie dei rendimenti a x anni veri
    med=median(indexDF.irrv[1:lastIndex])*100 # calcola la mediana del rendimento a x anni
    nameMed=@sprintf("Riferimento mediano (%.2f %%)",med)
    
    data_check=map(canonicalize,diff(indexDF.timestamp)) 
    println(indexName," max prices gap: ", maximum(data_check))
    #findall(x->x!=1)
            
    # tirare in avanti i valori di chiusura borsa per coprire i buchi (la borsa chiusa == prezzi invariati)

    index_v=vcat(0,diff(index)./index[1:end-1]).*100  # Variazione giornaliera percentuale
    p1=PlotlyJS.scatter(;x=index_t,y=index,color="blue",name=indexName,ylabel="Dollari")
    p2=PlotlyJS.scatter(;x=index_t,y=indexDF.medianRef,line_color="green",name=nameMed,ylabel="Dollari")
    return PlotlyJS.Plot([p1, p2])
    
#   
    #    trace1=scatter(x=[1:length(index_t)],y=index)
    #dualPlot(index_t,index,index_t,index_v,indexName,"","Index value [$indexCurrency]","Daily variation percent")
    #dualPlot(index_t,index,index_t,indexDF.medianRef,"","","Index value [$indexCurrency]","Median benchmark")
end

function longTermIndexRevenue(indexDB,N)
    indexDF=indexDB[3]; indexName=indexDB[4]
    index=indexDF.close; index_t=indexDF.timestamp
    nYears=Year(N)

    # Compute return rate after nYears for each index sample
    irrVect=[]
    irrParVect=[]
    maxVal=-1000
    minVal=1000
    for t in index_t
        if t<(index_t[end] - nYears) # if nYears of data after t are available
            indx=findall(tFin-> tFin >= t, index_t)[1]
            buy=index[indx]
            indx=findall(tFin-> tFin >= t+nYears, index_t)[1]
            sell=index[indx]
            #
            CAGR=(sell/buy) ^ (1/N) - 1
            maxVal=max(CAGR,maxVal)
            minVal=min(CAGR,minVal)
            irrVect=vcat(irrVect,CAGR)
            irrParVect=vcat(irrParVect,missing)
        else
            indx=findall(tFin-> tFin >= t, index_t)[1]
            buy=index[indx]
            sell=index[end]
            #Δt=index_t[end]-t # expressed in milliseconds
            Δt=yearfrac(Date(t),Date(index_t[end]),DayCounts.ActualActualISDA()) # expressed in years
            CAGR_PAR=(sell/buy) ^ (1/Δt) - 1
            irrVect=vcat(irrVect,missing)
            if(CAGR_PAR >= maxVal)
                CAGR_PAR=maxVal
            end
            if(CAGR_PAR <= minVal)
                CAGR_PAR=minVal
            end
            irrParVect=vcat(irrParVect,CAGR_PAR)
        end
    end
    
    indexDF[!,:irrv]=irrVect
    indexDF[!,:irrPv]=irrParVect
    return irrVect
end

function savePlot(plotHandle,name)
    # Save interactive plot
    open(name, "w") do io
        #Install local instance of Plotly
        #https://github.com/JuliaPlots/PlotlyJS.jl/issues/459
        #wget https://cdn.plot.ly/plotly-2.34.0.min.js
        #mv plotly-2.34.0.min.js plotly.min.js
        PlotlyBase.to_html(io, plotHandle, include_plotlyjs="directory",full_html=true)
        #PlotlyBase.to_html(io, plotHandle) 
    end
end

function longTermIndexRevenuePlot(indexDB,N)
    indexDF=indexDB[3]
    indexName=indexDB[4]

    irr10=scatter(x=indexDF.timestamp,y=indexDF.irrv*100,color="green")
    irr10minus=scatter(x=indexDF.timestamp,y=indexDF.irrPv*100,color="red")
    return PlotlyJS.Plot([irr10,irr10minus],
           Layout(title="$indexName: Revenue per year after $N years",
                  yaxis_title="Revenue Rate [%]",showlegend=false,
                  xaxis_rangeslider_visible=true))
end

function boxPlot(df,colX,colY,title,xlabel,ylabel)
    Y=df[!,colY]
    if colX==:none
        X=ones(size(Y))
        tickLabelsCond=false
    else
        X=df[!,colX]
        tickLabelsCond=true
    end
    layout=Layout(; title=title,
                  xaxis=attr(;showgrid=false, zeroline=false, title=xlabel, showticklabels=tickLabelsCond),
                  yaxis=attr(;title=ylabel))

    return PlotlyJS.Plot(X, Y, boxpoints="all", kind="box", layout)
end


############################
#% OBBLIGAZIONI ZERO CUPON #
############################
prezzo_acquisto=92.47/100
prezzo_vendita=100/100
capitale_da_investire=40*1000 #[euro]
data_acquisto=oggi
data_vendita=Date("01-11-2026", dateformat"d-m-y")
aliquota_fiscale=12.5/100 # NB: minusvalenza rendicontata come azioni, plusvalenza come reddito da capitale 
#--------------------------
rendimento_acquisto=1-prezzo_acquisto
controvalore_acquistato=capitale_da_investire * 1/(1-rendimento_acquisto);
capitale_finale_lordo=controvalore_acquistato*prezzo_vendita
plusvalenza=capitale_finale_lordo-capitale_da_investire
if(plusvalenza > 0)
    tasse=plusvalenza*aliquota_fiscale
else
    tasse=0
end
capitale_finale_netto=capitale_finale_lordo-tasse
#
flussi_di_cassa = [-capitale_da_investire, capitale_finale_netto]
delta_anni_flussi = [0, Dates.value((data_vendita-data_acquisto))/365]
#
TIR_ZC=irr(flussi_di_cassa,delta_anni_flussi)
println("\nTIR Obbligazione Zero Cupon: ",trunc(TIR_ZC.value*100,digits=2),"% netto")


#####################################################
#% OBBLIGAZIONI CON CEDOLA FISSA PORTATE A SCADENZA #
#####################################################
prezzo_acquisto=109.21/100
prezzo_emissione=101/100
capitale_da_investire=40*1000 #[euro]
data_acquisto=oggi
data_scadenza=Date("01-11-2027", dateformat"d-m-y")
tasso_annuale=7.25/100 
n_cedole_annuali=1
aliquota_fiscale=12.5/100
#---------------------------------------------------
rendimento_acquisto=1-prezzo_acquisto
controvalore_acquistato=capitale_da_investire * 1/(1-rendimento_acquisto)
anni_rimanenti=Dates.value((data_scadenza-data_acquisto))/365
#
n_cedole_rimanenti=floor(Int,anni_rimanenti*n_cedole_annuali)
periodo_intercedolare=Dates.Month(floor(12/n_cedole_annuali));
tasso_cedolare=tasso_annuale/n_cedole_annuali
cedola=controvalore_acquistato*tasso_cedolare*(1-aliquota_fiscale)
cedole=cedola*ones(n_cedole_rimanenti-1)
offset_scadenza=Dates.value((data_scadenza-data_acquisto))/365
delta_anni_cedole= offset_scadenza .- map(Dates.value,collect((n_cedole_rimanenti-1):-1:1)*periodo_intercedolare)/12
#
tassa_plusvalenza = (1 - prezzo_emissione) * controvalore_acquistato * aliquota_fiscale
if (tassa_plusvalenza < 0)
    tassa_plusvalenza=-trunc(tassa_plusvalenza,digits=2)
    print("Genera una minusvalenza utile a scontare $tassa_plusvalenza euro di tasse")
    tassa_plusvalenza = 0
end
restituzione_capitale = controvalore_acquistato - tassa_plusvalenza
flussi_di_cassa = vcat(-capitale_da_investire, cedole,restituzione_capitale+cedola)
delta_anni_flussi = vcat(0, delta_anni_cedole, offset_scadenza)
#
TIR_OB=irr(flussi_di_cassa,delta_anni_flussi)
println("\nTIR Obbligazione a Scadenza: ",trunc(TIR_OB.value*100,digits=2),"% netto")



###############################################
#% RENDIMENTO A LUNGO TERMINE INDICI AZIONARI #
###############################################
#MSCI AC ASIA PACIFIC (^302000-USD-STRD)
#% COLLECT HISTORICAL DATA                  (!!!!!! CREATE DB DATA STRUCTURE !!!!!!)
#ni_d  = Date("05-02-1471", dateformat"d-m-y") # New Index Initial Date
#ni_id = "GC=F" # New Index Identifier
#ni_DF = get_prices(ni_id,interval="1d",startdt=ni_d, enddt=Dates.today()) |> DataFrame 
#push!(pricesDB, (ni_id,"1d",ni_DF,"GOLD","USD") )
#save_object("HistoricalPrices.jld2", pricesDB)
#%
#pricesDB=[("^GSPC","1d",ni_DF,"S&P 500")]
#for i in [1,2,3,4]
#    pricesDB[i]=(pricesDB[i][1],pricesDB[i][2],pricesDB[i][3],pricesDB[i][4],"USD")
#end
#save_object("HistoricalPrices.jld2", pricesDB)
#
pricesDB=load_object("HistoricalPrices.jld2");
pricesDB=update_pricesDB(pricesDB)
pricesDBmod=deepcopy(pricesDB)

# fix world data
world=pricesDBmod[4][3]
worldFixed=filter(:close => x -> !(ismissing(x) || isnothing(x) || isnan(x)),world)
pricesDBmod[4]=(pricesDBmod[4][1],pricesDBmod[4][2],worldFixed,pricesDBmod[4][4],pricesDBmod[4][5])

# fix gold data
gold=pricesDBmod[5][3]
goldFixed=filter(:close => x -> !(ismissing(x) || isnothing(x) || isnan(x)),gold)
pricesDBmod[5]=(pricesDBmod[5][1],pricesDBmod[5][2],goldFixed,pricesDBmod[5][4],pricesDBmod[5][5])

#% CONFIGURE PLOTS
revStepSP500=20
revStepBitcoin=10
revStepNasdaq=20
revStepMSCIWorld=20
revStepGold=20

#% ELABORATE DATA
longTermIndexRevenue(pricesDBmod[1],revStepSP500)
longTermIndexRevenue(pricesDBmod[2],revStepBitcoin)
longTermIndexRevenue(pricesDBmod[3],revStepNasdaq)
longTermIndexRevenue(pricesDBmod[4],revStepMSCIWorld)
longTermIndexRevenue(pricesDBmod[5],revStepGold)

#% compute benchmarks
medianBenchmark(pricesDBmod[1])
medianBenchmark(pricesDBmod[2])
medianBenchmark(pricesDBmod[3])
medianBenchmark(pricesDBmod[4])
medianBenchmark(pricesDBmod[5])

#% VISUALIZE DATA (!!! FINIRE IL CHECK SALTI TEMPORALI !!)

# Plot index
p=plotIndex(pricesDBmod[1])#sp500
savePlot(p,"Reports/SP500.html")
p=plotIndex(pricesDBmod[2])#bitcoin
savePlot(p,"Reports/Bitcoin.html")
p=plotIndex(pricesDBmod[3])#nasdaq100
savePlot(p,"Reports/Nasdaq.html")
p=plotIndex(pricesDBmod[4])#msci world
savePlot(p,"Reports/MSCI_World.html")
p=plotIndex(pricesDBmod[5])#gold
savePlot(p,"Reports/Gold.html")

#plot(pricesDBmod[2][3].timestamp,pricesDBmod[2][3].irrv,Layout(yaxis_type="log")) |> display


# Revenue Plots
p=longTermIndexRevenuePlot(pricesDBmod[1],revStepSP500)
savePlot(p,"Reports/SP500_R$revStepSP500.html")
p=longTermIndexRevenuePlot(pricesDBmod[2],revStepBitcoin)
savePlot(p,"Reports/Bitcoin_R$revStepBitcoin.html")
p=longTermIndexRevenuePlot(pricesDBmod[3],revStepNasdaq)
savePlot(p,"Reports/Nasdaq_R$revStepNasdaq.html")
p=longTermIndexRevenuePlot(pricesDBmod[4],revStepMSCIWorld)
savePlot(p,"Reports/MSCI_World_R$revStepMSCIWorld.html")
p=longTermIndexRevenuePlot(pricesDBmod[5],revStepGold)
savePlot(p,"Reports/Gold_R$revStepGold.html")

# Box Plots
p=boxPlot(pricesDBmod[1][3],:none,:irrv,pricesDBmod[1][4],"","Percent [0-1]")
savePlot(p,"Reports/SP500_B$revStepSP500.html")
p=boxPlot(pricesDBmod[2][3],:none,:irrv,pricesDBmod[2][4],"","Percent [0-1]")
savePlot(p,"Reports/Bitcoin_B$revStepBitcoin.html")
p=boxPlot(pricesDBmod[3][3],:none,:irrv,pricesDBmod[3][4],"","Percent [0-1]")
savePlot(p,"Reports/Nasdaq_B$revStepNasdaq.html")
p=boxPlot(pricesDBmod[4][3],:none,:irrv,pricesDBmod[4][4],"","Percent [0-1]")
savePlot(p,"Reports/MSCI_World_B$revStepMSCIWorld.html")
p=boxPlot(pricesDBmod[5][3],:none,:irrv,pricesDBmod[5][4],"","Percent [0-1]")
savePlot(p,"Reports/Gold_B$revStepGold.html")

# CALCOLARE BEST FIT DEI DATI PER VEDERE IL RENDIMENTO PIU PROBABILE
# plottarlo e scriverne il valore in legenda

# Rivalutato=Iniziale*(1+rendimento)^intervallo => X+1 = X * (1+r)^t
# Per semplificare il problema t=1 giorno e R=rendimento giornaliero
# X(k+1)=X(k)*R AND X(0)=iniziale => X(k)=R^k*X(0)
# Convertire rendimento giornaliero in annuale
# 



#plottare index RO e capire perche sia infeasible

# y=Index1d, x(0)=CapitaleIniziale
# ARGOMENTI
#y=pricesDBmod[1][3].close[1:100]
#index=[1,2,5,10,30,80,100,200,500,1000,1800,10000]
## FUNZIONE
#
#using TSSOS,COSMO,DynamicPolynomials # RO
#using Plots; gr(); default(show=true,minorgrid=true,dpi=300)
#
#function exponentialFit(index)
##    index=indexDF[3].close
#    x0=index[1]
#    y=index[2:end]
#
#    # Optimization variables declaration
#    @polyvar x[1:length(y)]
#    @polyvar errNorm[1:length(y)]
#    @polyvar r
#    optimVars = vcat(x,errNorm,r)
#
#    # Objective function [ min obj ]
#    obj=reduce(+,errNorm)
#
#    # Dynamic constraint [ constr = 0 ]
#    equalities=Polynomial{true,Float64}[];
#    push!(equalities,x0*r-x[1]) # x(1)=x0*R
#    for index in 2:length(x)-1
#        eqnK=x[index]*r-x[index+1] # x(k+1)=x(k)*R
#        push!(equalities, eqnK);
#    end
#
#    # Norm constraints [ constr >= 0 ]
#    err=(y-x)./y # Normalized error
#    ubConstr=errNorm - err  #y(k)-x(k)<=norm(k)
#    lbConstr=err + errNorm  #y(k)-x(k)>=-norm(k)
#    positiveness=errNorm    # the norm of the normalized error should be positive
#    disequalities=vcat(ubConstr,lbConstr,positiveness)
#
#    # Build Polynomial Optimization Problem
#    pop = vcat(obj,disequalities,equalities)
#    d = 2 # the relaxation order
#    opt,sol,data = tssos_first(pop, optimVars, d, numeq=length(equalities),
#                               TS=false, solver="COSMO", solution=true)
#    #indexDF[3][!,:expFit]=vcat(x0,sol[1:lenght(x)])
#    #solLabels=zip(optimVars,sol) |> collect
#    return vcat(x0,sol[1:length(x)])
#    #return solLable
#end
#
#sol=exponentialFit(index)
#plot([1:length(index)],index,label="index")
#plot!([1:length(index)],sol,label="fit") |> display

# min Σ[ norm(k) ] ∀k
#    s.t.
#        x(k+1)=x(k)*R, ∀k
#        y(k)-x(k)<=norm(k), ∀k
#        y(k)-x(k)>=-norm(k), ∀k
#  x,norm>= 0


#% Fine script

# aggiungi monero

#   CALCOLARE CLOSEcont e tCONT dove t e' davvero uguale ad 1 giorno!!

# best fit unidimenzionale
# 1) f(dati,precision)
# 2) inizio da rTMP=0, rBEST=0 e counter=0
# 3) for counter in [0,precision]
# 4)    for iter in 1:10
# 5)       rTMP=rTMP+iter*1^counter
# 6)       calcola evoluzione (modificare medianBenchmark e renderla 2 funzioni,
#                              una indiceDF e una semplice da usare qui)
# 7)       calcolare errore giorno per giorno tra evoluzione e indice reale
# 8)       calcolare RME
# 9)       if RME < RME_best then rBest=rTMP else break

# ASSOLUTAMENTE SALVARE SU DISCO INVECE DI SPARARE GRAFICI A CASO
