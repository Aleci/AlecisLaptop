# https://juliacontrol.github.io/ControlSystems.jl/dev/man/differences/#Precompilation-for-faster-load-times

first_install=false

if first_install
    using Pkg
    Pkg.add("ControlSystems")
    Pkg.add("Plots")
    Pkg.add("PackageCompiler")
end

using PackageCompiler

PackageCompiler.create_sysimage(
    [
        :ActuaryUtilities,
        :Dates,
        :DayCounts,
        :YFinance,
        :PlotlyJS,
        :Statistics,
        :DataFrames,
        :JLD2,
        :Debugger
    ];
    precompile_execution_file = "Finanza.jl",
    sysimage_path = "sys_Finance_$(VERSION).so",
)
exit()

# Then run julia each time with:
# user@system:$ julia -J sys_ControlSystems_<VERSION>.so
# alias juliaControl="julia -J sys_ControlSystems_<VERSION>.so"
