#!/bin/bash
# A useful tool for managing file duplicates among backups is "fdupes -drS"

# CLI disabled requires passing arguments to the script
ENABLE_CLI=1; #1. enable; 0. disable

#===== PATHS CONFIGURATION ===================================================

DRIVE1=PATH/TO/REPO/DRIVE
REPO=$DRIVE1/PATH/TO/REPO/FOLDER/  # <- Remember to add the trailing slash
REPO_DESC="REPO"

DRIVE2=  PATH/TO/LOCAL/DRIVE
LOCAL=$DRIVE2/PATH/TO/LOCAL/FOLDER  # <- Remember to add the trailing slash
LOCAL_DESC="LOCAL"
#=============================================================================

if [ $ENABLE_CLI -eq 1 ]; then
    echo "====================== BACKUP TOOL ======================"
    echo "Write the selected option"
    echo "pull: $LOCAL_DESC <= $REPO_DESC"
    echo "push: $LOCAL_DESC => $REPO_DESC"
    echo "quit: Exit program"
    read -p ">>> " CMD
else
    CMD=$1; # if the CLI is not enabled the command must be the first argument
fi

#Backup if mounted
if [ -e $DRIVE1 ] && [ -e $DRIVE2 ]; then
    
    #Pull only if explicitly requested
    if [[ $CMD == "pull" ]] ; then 
	# Simple version
	#rsync -al --delete --info=progress2 $REPO/ $LOCAL/
	# Optimized version
	cd $REPO
        rsync -cdlptgov --delete . /$LOCAL
        find . -maxdepth 1 -type d -not -name "." -exec rsync -crlptgov --delete {} /$LOCAL \;
	echo "Done: $REPO_DESC -> $LOCAL_DESC"
	
    #Push only if explicitly requested
    else if [[ $CMD == "push" ]] ; then
	     # Simple version
	     #rsync -al --delete --info=progress2 $LOCAL/ $REPO/
	     # Optimized version
             cd $LOCAL
             rsync -cdlptgov --delete . /$REPO
             find . -maxdepth 1 -type d -not -name "." -exec rsync -crlptgov --delete {} /$REPO \; 
	     echo "Done: $LOCAL_DESC -> $REPO_DESC"
	 else
	     echo "Quit without changes"
	 fi
    fi
else
    echo "At least one drive has not been found"
fi

echo ""
read -s -n 1 -p "Press any key to continue . . ."
echo ""
