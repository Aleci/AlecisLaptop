#!/bin/bash

#Print man
if [[ $1 == "help" ]] ; then
    echo "pull: Aleci -> Hard drive"
    echo "push: Hard drive -> Aleci"
else
    #Backup Aleci if mounted
    if [ -e /media/$USER/Aleci ] ; then

	#Pull only if explicitly requested (Pen drive -> Hard drive)
	if [[ $1 == "pull" ]] ; then
	    chmod -R 777 $HOME/Aleci
	    rsync -al --delete /media/$USER/Aleci/ $HOME/Aleci
	    chmod -R 775 $HOME/Aleci
	    echo "Aleci         DONE"
	    
	#Push only if explicitly requested (Hard drive -> Pen drive)
	else if [[ $1 == "push" ]] ; then
		 chmod -R 777 /media/$USER/Aleci
		 rsync -al --delete $HOME/Aleci/ /media/$USER/Aleci
		 chmod -R 775 /media/$USER/Aleci
		 echo "Aleci         DONE"
	     else
		 echo "Aleci         READY"
	     fi
	fi
    else
	echo "Aleci         SKIP"
    fi
fi


