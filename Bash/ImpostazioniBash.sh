#Imposto colori di ls
export LS_COLORS='no=0:di=01;34:ln=04:or=04;33:ex=01;32:mi=04;33:*.tar=01;33:*.gz=01;33:*.zip=01;33:*.rar=01;33:'

#Imposto variabili colore globali
export BIANCO='\e[0;37m'
export BLU='\e[0;34m'
export VERDE='\e[0;32m'
export AZZURRO='\e[0;36m'
export ROSSO='\e[0;31m'
export GIALLO='\e[0;33m'
export MARRONE='\e[0;33m'
export GRIGIO='\e[0;30m'
export VIOLA='\e[0;35m'

#Imposto starter
if [ $EUID -ne 0 ] 
  then PS1=''$VERDE'\u'$BIANCO'@\h'$GIALLO'\w'$BIANCO':\n\A \$ '
  else PS1=''$ROSSO'\u'$BIANCO'@\h'$GIALLO'\w'$BIANCO':\n\A \$ '
fi

#Imposto alias personalizzati
alias em='emacs -nw'
alias la='ls -A'
alias lal='ls -la'
alias ll='ls -l'
alias ls='ls -p --color=auto'
alias reboot='sudo reboot'
alias halt='sudo halt'
alias sl='ls'
alias gitlog='git log --oneline --all --graph --decorate'
alias ml='matlab -nodisplay'
alias cm='cmatrix -nabsu 3'
alias vaffanculo='espeak -v it "Ma vaffanculo tu, imbecille. Guarda un po questo qua che non sa fa le cose e se la pia con me"'
alias fanculo='Fanculo a te!'

#Imposto path stack haskell
#export PATH="$PATH:/home/ale/.local/bin"

