####################### GUIX #############################

# Detect hybrid system
if [ "uname -v | grep Debian" ] ; then
    HYBRID=true; 
else
    HYBRID=false;
fi

# Load current guix package manager
if [ -d "$HOME/.config/guix/current" ] ; then
    export GUIX_PROFILE="$HOME/.config/guix/current"
    source "$GUIX_PROFILE/etc/profile"
    if $HYBRID ; then
	export PATH="$HOME/.config/guix/current/bin:$PATH"
    fi
fi

# Load default profile
if [ -d "$HOME/.guix-profile" ] ; then
    GUIX_PROFILE="/home/ale/.guix-profile"
    source "$GUIX_PROFILE/etc/profile"
    if $HYBRID ; then
	export PATH="$GUIX_PROFILE/bin:$PATH"
    fi
fi

# Set locale on hybrid system
if $HYBRID ; then
    export GUIX_LOCPATH=$GUIX_PROFILE/lib/locale
fi

# Load other installed profiles
GUIX_EXTRA_PROFILES=$HOME/.guix-extra-profiles
if [ -d "$GUIX_EXTRA_PROFILES" ] ; then
    for i in $GUIX_EXTRA_PROFILES/*; do
	profile=$i/$(basename "$i")
	if [ -f "$profile"/etc/profile ]; then
            GUIX_PROFILE="$profile"
            source "$GUIX_PROFILE"/etc/profile
	    if $HYBRID ; then
		export PATH="$profile/bin:$PATH"
	    fi
	fi
	unset profile
    done
fi

######################## BASH #############################

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

###################### OTHER ##############################

# Load ite keyboard
xkbcomp $HOME/.xkb/ite $DISPLAY 2> /dev/null
